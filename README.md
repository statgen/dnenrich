Software for assessment of statistical enrichment of de novo mutations in gene sets.

## Web site: ##
# **https://fromem03.u.hpc.mssm.edu/dnenrich** #


## Alternatively-hosted web site (if site above is down): ##
# **https://statgen.bitbucket.io/dnenrich** #



![dnenrich_dartboard.png](https://bitbucket.org/repo/LrLooM/images/3670278451-dnenrich_dartboard.png)